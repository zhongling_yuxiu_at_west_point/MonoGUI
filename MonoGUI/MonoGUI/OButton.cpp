////////////////////////////////////////////////////////////////////////////////
// @file OButton.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OButton::OButton ()
	: OWindow(self_type)
{
}

OButton::~OButton ()
{
}

BOOL OButton::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		return FALSE;
	}

	return TRUE;
}

// 虚函数，绘制按钮
void OButton::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible())
		return;

	// 计算文字的居中显示位置
	int tx = m_x + (m_w - 6 * GetTextLength()) / 2;
	int ty = m_y + (m_h - 12) / 2;

	// 根据按钮不同的状态绘制
	// 如果是按下状态，则按照按下状态绘制
	if ((m_wStatus & WND_STATUS_PUSHED) > 0)
	{
		int crBk = 0;

		if ((m_wStyle & WND_STYLE_ROUND_EDGE) > 0)
		{
			// 绘制圆角风格的按钮
			pLCD->FillRect (m_x+6, m_y, m_w-12, m_h, crBk);
			pLCD->FillRect (m_x, m_y+6, 6, m_h-12, crBk);
			pLCD->FillRect (m_x+m_w-6, m_y+6, 6, m_h-12, crBk);

			if (crBk == 0) {
				pLCD->DrawImage(m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_BLACKNESS);
				pLCD->DrawImage(m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_BLACKNESS);
				pLCD->DrawImage(m_x, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 0, 6, LCD_MODE_BLACKNESS);
				pLCD->DrawImage(m_x+m_w-6, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 6, 6, LCD_MODE_BLACKNESS);
			}
			else {
				pLCD->DrawImage(m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_WHITENESS);
				pLCD->DrawImage(m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_WHITENESS);
				pLCD->DrawImage(m_x, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 0, 6, LCD_MODE_WHITENESS);
				pLCD->DrawImage(m_x+m_w-6, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 6, 6, LCD_MODE_WHITENESS);
			}
		}
		else
		{
			// 绘制普通风格的按钮
			pLCD->FillRect (m_x, m_y, m_w, m_h, crBk);
		}
		// 绘制文字
		pLCD->TextOut (tx,ty,(BYTE*)m_sCaption,WINDOW_CAPTION_BUFFER_LEN-1, LCD_MODE_INVERSE);
		return;
	}

	// 如果是默认的，则绘制宽边按钮。如果不是默认，则绘制普通按钮
	if ((m_wStatus & WND_STATUS_DEFAULT) > 0)
	{
		int crBk = 1;
		int crFr = 0;

		// 绘制默认风格的按钮
		if ((m_wStyle & WND_STYLE_ROUND_EDGE) > 0)
		{
			// 绘制圆角风格的按钮外框
			pLCD->FillRect (m_x+6, m_y+1, m_w-12, m_h-2, crBk);
			pLCD->FillRect (m_x+1, m_y+6, 5, m_h-12, crBk);
			pLCD->FillRect (m_x+m_w-6, m_y+6, 5, m_h-12, crBk);
			pLCD->DrawImage(m_x, m_y, 6, 6, g_4color_Button_Default, 0, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y, 7, 6, g_4color_Button_Default, 6, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x, m_y+m_h-6, 6, 7, g_4color_Button_Default, 0, 6, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y+m_h-6, 7, 7, g_4color_Button_Default, 6, 6, LCD_MODE_NORMAL);
			pLCD->HLine    (m_x+6, m_y, m_w-12, crFr);
			pLCD->VLine    (m_x, m_y+6, m_h-12, crFr);
			pLCD->FillRect (m_x+6, m_y+m_h-1, m_w-12, 2, crFr);
			pLCD->FillRect (m_x+m_w-1, m_y+6, 2, m_h-12, crFr);
		}
		else
		{
			// 绘制普通风格的按钮外框
			pLCD->FillRect(m_x, m_y, m_w, m_h, crBk);
			pLCD->HLine   (m_x, m_y, m_w, crFr);
			pLCD->HLine   (m_x, m_y+m_h-1, m_w, crFr);
			pLCD->VLine   (m_x, m_y, m_h, crFr);
			pLCD->VLine   (m_x+m_w-1, m_y, m_h, crFr);
			pLCD->HLine   (m_x+1, m_y+m_h, m_w, crFr);
			pLCD->VLine   (m_x+m_w, m_y+1, m_h, crFr);
		}
	}
	else
	{
		int crBk = 1;
		int crFr = 0;

		// 绘制普通风格的按钮
		if ((m_wStyle & WND_STYLE_ROUND_EDGE) > 0)
		{
			// 绘制圆角风格的按钮外框
			pLCD->FillRect (m_x+6, m_y+1, m_w-12, m_h-2, crBk);
			pLCD->FillRect (m_x+1, m_y+6, 5, m_h-12, crBk);
			pLCD->FillRect (m_x+m_w-6, m_y+6, 5, m_h-12, crBk);
			pLCD->DrawImage(m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 0, 6, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 6, 6, LCD_MODE_NORMAL);
			pLCD->HLine    (m_x+6, m_y, m_w-12, crFr);
			pLCD->VLine    (m_x, m_y+6, m_h-12, crFr);
			pLCD->HLine    (m_x+6, m_y+m_h-1, m_w-12, crFr);
			pLCD->VLine    (m_x+m_w-1, m_y+6, m_h-12, crFr);
		}
		else
		{
			// 绘制普通风格的按钮外框
			pLCD->FillRect(m_x, m_y, m_w, m_h, crBk);
			pLCD->HLine   (m_x, m_y, m_w, crFr);
			pLCD->HLine   (m_x, m_y+m_h-1, m_w, crFr);
			pLCD->VLine   (m_x, m_y, m_h, crFr);
			pLCD->VLine   (m_x+m_w-1, m_y, m_h, crFr);
		}
	}
	// 如果处于焦点，再绘制虚线框
	if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
	{
		int crFr = 2;

		if ((m_wStyle & WND_STYLE_ROUND_EDGE) > 0)
		{
			// 圆角按钮，虚线框要避开四角的圆弧
			pLCD->FillRect (m_x+3,m_y+2,m_w-7,1,crFr);
			pLCD->FillRect (m_x+2,m_y+3,1,m_h-7,crFr);
			pLCD->FillRect (m_x+3,m_y+m_h-3,m_w-7,1,crFr);
			pLCD->FillRect (m_x+m_w-3,m_y+3,1,m_h-7,crFr);
		}
		else
		{
			pLCD->FillRect (m_x+2,m_y+2,m_w-5,1,crFr);
			pLCD->FillRect (m_x+2,m_y+2,1,m_h-5,crFr);
			pLCD->FillRect (m_x+2,m_y+m_h-3,m_w-5,1,crFr);
			pLCD->FillRect (m_x+m_w-3,m_y+2,1,m_h-5,crFr);
		}
	}
	// 绘制正常的文字
	pLCD->TextOut (tx,ty,(BYTE*)m_sCaption,WINDOW_CAPTION_BUFFER_LEN-1, LCD_MODE_NORMAL);
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int OButton::Proc (OWindow* pWnd, ULONGLONG iMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int iReturn = 0;

	// 按钮只处理三种消息：1、空格键；2、回车键(只对处于默认状态的按钮有效)；3、OM_PUSHDOWN消息
	if (iMsg == OM_KEYDOWN)
	{
		if (wParam == KEY_SPACE)
		{
			if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
			{
				OnPushDown ();
			}
			iReturn = 1;
		}
		else if (wParam == KEY_ENTER)
		{
			if ((m_wStatus & WND_STATUS_DEFAULT) > 0)
			{
				OnPushDown ();
			}
			iReturn = 1;
		}
	}
	else if (iMsg == OM_PUSHDOWN)
	{
		if (pWnd == this)
		{
			OnPushDown ();
		}
		iReturn = 1;
	}

	return iReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OButton::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int iReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nMsg == OM_LBUTTONDOWN)
	{
		if (PtInWindow (wParam, lParam))
		{
			OnPushDown();
			iReturn = 1;
		}
	}

	return iReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 按键被按下的处理
void OButton::OnPushDown ()
{
	CHECK_TYPE;

	DebugPrintf ("OnPushDown \n");
	// 刷新显示，向父窗口发出通知消息，再次刷新显示
	m_wStatus |= WND_STATUS_PUSHED;
	Paint(m_pApp->m_pLCD);

	// 强制显示按钮更新的效果
	m_pApp->Show ();

	// 延时，使按钮按下的状态可以被看清楚
	Delay (200);

	// 向父窗口发送消息值为本ID的消息
	O_MSG msg;
	msg.pWnd    = m_pParent;
	msg.message = OM_NOTIFY_PARENT;
	msg.wParam  = m_ID;
	msg.lParam  = 0;
	m_pApp->PostMsg (&msg);

	m_wStatus &= ~WND_STATUS_PUSHED;
	Paint (m_pApp->m_pLCD);

	// 强制显示按钮更新的效果
	m_pApp->Show ();
}

/* END */