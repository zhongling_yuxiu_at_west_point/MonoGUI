////////////////////////////////////////////////////////////////////////////////
// @file OWindow.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OWindow::OWindow(WORD wWndType)
	: m_wWndType(wWndType)
{
	m_nTabOrder		= 0x0;
	m_pVScroll		= NULL;
	m_pHScroll		= NULL;
	m_dwAddData1	= 0x0;
	m_dwAddData2	= 0x0;
	m_dwAddData3	= 0x0;
	m_dwAddData4	= 0x0;
	memset (m_sCaption, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	m_pApp			= NULL;
	m_pParent		= NULL;
	m_nChildCount	= 0;
	m_pChildren		= NULL;
	m_pActive		= NULL;
	m_pDefaultButton= NULL;
	m_pNext			= NULL;
	m_pPrev			= NULL;
	m_pOldParentActiveWindow = NULL;
#if defined (MOUSE_SUPPORT)
	m_nMouseMoving   = 0;
#endif // defined(MOUSE_SUPPORT)
}

OWindow::~OWindow()
{
	if (m_pVScroll != NULL)
	{
		delete m_pVScroll;
		m_pVScroll = NULL;
	}

	if (m_pHScroll != NULL)
	{
		delete m_pHScroll;
		m_pHScroll = NULL;
	}

	// 删除子窗口链表
	if (m_nChildCount == 0) {
		return;
	}

	OWindow* pNext = m_pChildren->m_pNext;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if (m_pChildren != NULL)
		{
			delete m_pChildren;
			m_pChildren = NULL;
		}
		if (pNext != NULL)
		{
			m_pChildren = pNext;
			pNext = m_pChildren->m_pNext;
		}
		else
		{
			return;
		}
	}
}

// 创建一个窗口；
// 如果父窗口不是NULL，则把该窗口插入父窗口的字窗口链表中；
// tab序号根据父窗口的字窗口数目确定；
// 快捷键列表、脱字符、滚动条、题头文字可以在窗口创建后再进行设置；
BOOL OWindow::Create
(
	OWindow* pParent,			// 父窗口指针
	WORD wStyle,				// 窗口的样式
	WORD wStatus,				// 窗口的状态
	int x,
	int y,
	int w,
	int h,						// 绝对位置
	int ID						// 窗口的ID号
)
{
	DebugPrintf("Debug: OWindow::Create wWndType is: %s\n",
		GetWindowTypeName(GetWndType()));
	m_wStyle	= wStyle;
	m_wStatus	= wStatus;
	m_x			= x;
	m_y			= y;
	m_w			= w;
	m_h			= h;
	m_ID		= ID;

	// 如果父窗口不是空的，则将自己挂在父窗口的子窗口链表中，并设置相应的参数
	if (pParent == NULL)
	{
		if (m_wWndType == WND_TYPE_DESKTOP)
		{
			if (NULL == m_pApp)
			{
				DebugPrintf("Debug: OWindow::Create() cannot run before OApp::Create()\n");
				return FALSE;
			}
			goto NORMAL_RETRURN;
		}
		else
		{
			DebugPrintf("Debug: OWindow::Create pParent==NULL and IS NOT the Desktop.\n");
			return FALSE;
		}
	}

	m_pParent = pParent;
	m_pApp = pParent->m_pApp;
	m_pOldParentActiveWindow = pParent->m_pActive;
	m_nTabOrder= pParent->m_nChildCount;

	if (pParent->m_nChildCount == 0)
	{
		m_pPrev = NULL;
		m_pNext = NULL;
		m_pParent->m_pChildren = this;
		pParent->m_nChildCount ++;
	}
	else
	{
		m_pNext = pParent->m_pChildren;

		if (pParent->m_pChildren->m_pPrev != NULL)
		{
			m_pPrev = pParent->m_pChildren->m_pPrev;
			m_pNext = pParent->m_pChildren;
			pParent->m_pChildren->m_pPrev->m_pNext = this;
			pParent->m_pChildren->m_pPrev = this;
		}
		else
		{
			m_pPrev = pParent->m_pChildren;
			m_pNext = pParent->m_pChildren;
			pParent->m_pChildren->m_pPrev = this;
			pParent->m_pChildren->m_pNext = this;
		}

		pParent->m_nChildCount ++;
	}

	// 如果本窗口是按钮且具有默认属性，则修改父窗口的m_pDefaultButton
	if (( ((m_wWndType & WND_TYPE_BUTTON) > 0)
		||((m_wWndType & WND_TYPE_IMAGE_BUTTON) > 0) )
		&&((m_wStatus & WND_STATUS_DEFAULT) > 0))
	{
		pParent->m_pDefaultButton = this;
	}

	// 如果本窗口具有焦点属性(对话框或者第一个控件)，则向父窗口发送消息将本窗口设置为焦点
	if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
	{
		O_MSG msg;
		msg.pWnd	= m_pParent;
		msg.message = OM_SETCHILDACTIVE;
		msg.wParam  = m_ID;
		msg.lParam  = 0;
		m_pApp->SendMsg (&msg);
	}

NORMAL_RETRURN:

	// 发送OM_INITWINDOW消息，该消息将引发OnInit处理。
	O_MSG msg;
	msg.pWnd	= this;
	msg.message = OM_INITWINDOW;
	msg.wParam  = 0;
	msg.lParam  = 0;
	m_pApp->PostMsg (&msg);

	return TRUE;
}

// 虚函数，绘制窗口，绘制附加的滚动条
// 重载此函数，请在最后调用该函数
void OWindow::Paint (LCD* pLCD)
{
	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	// 绘制附加的滚动条
	if((m_wStyle & WND_STYLE_NO_SCROLL) == 0)
	{
		if (m_pVScroll != NULL) {
			m_pVScroll->Paint (pLCD);
		}
		if (m_pHScroll != NULL) {
			m_pHScroll->Paint (pLCD);
		}
	}

	// 然后，调用各个子窗口的绘制函数
	OWindow* pNext = m_pChildren;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if (pNext == NULL) {
			return;
		}
		//	DebugPrintf( "Debug: OWindow::Paint: pNext->m_wWndType = %s \n", GetWindowTypeName(pNext->m_wWndType) );
		//	DebugPrintf( "Debug: OWindow::Paint: pNext->m_cCaption = %s \n", pNext->m_cCaption );
		pNext->Paint (pLCD);
		pNext = pNext->m_pNext;
	}
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
// 如果重载此函数，请首先调用该函数，然后再进行其他处理
int OWindow::Proc (OWindow* pWnd, ULONGLONG iMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	if (! IsWindowEnabled()) {
		return 0;
	}

	int iReturn = 0;

	// 沿焦点路径发送的消息，如果已经被处理，则后续的窗口不再处理。
	// 首先调用焦点窗口的处理函数
	if (m_pActive != NULL)
	{
		if (m_pActive->Proc (pWnd, iMsg, wParam, lParam) == 1)
			iReturn = 1;
	}

	// 如果焦点窗口未做处理，交给默认按钮处理
	if (iReturn != 1)
	{
		if (m_pDefaultButton != NULL)
		{
			if (m_pDefaultButton->Proc (pWnd, iMsg, wParam, lParam) == 1)
				iReturn = 1;
		}
	}
	
	// 针对特定本窗口进行的处理，
	// 无论是否处理了，均不改变iReturn的值。
	if (pWnd == this)
	{
		switch (iMsg)
		{
		case OM_INITWINDOW:
			OnInit();
			break;

		case OM_TIMER:
			OnTimer (wParam, lParam);
			break;

		case OM_CLOSE:
			{
				// 调用自己的OnClose函数
				BOOL bClose = OnClose ();
				// 向父窗口发送消息销毁自己
				if (bClose)
				{
					// 首先，向子窗口发送失去焦点的消息
					// 主要是OEdit控件，需要在失去焦点的时候关闭输入法，隐藏脱字符
					O_MSG msg;

					if (m_nChildCount != 0)
					{
						msg.message = OM_KILLFOCUS;
						msg.lParam  = 0;
						OWindow* pNext = m_pChildren;
						int i;
						for (i = 0; i < m_nChildCount; i++)
						{
							if (pNext != NULL)
							{
								msg.pWnd   = pNext;
								msg.wParam = pNext->m_ID;
								m_pApp->SendMsg (&msg);
								pNext = pNext->m_pNext;
							}
						}
					}

					msg.pWnd    = m_pParent;
					msg.message = OM_DELETECHILD;
					msg.wParam  = m_ID;
					if (IsWindowActive()) {
						msg.lParam = (ULONGLONG)m_pOldParentActiveWindow;
					}
					else {
						msg.lParam = 0;
					}
					m_pApp->PostMsg (&msg);
				}
			}
			break;

		case OM_DATACHANGE:
			{
				OnDataChange (wParam);

				// 向桌面窗口发出重绘该子窗口的消息
				OWindow* pWnd = FindChildByID (wParam);
				O_MSG msg;
				msg.pWnd	= pWnd;
				msg.message = OM_KILLFOCUS;
				msg.wParam  = wParam;
				msg.lParam  = 0;
				m_pApp->SendMsg (&msg);
			}
			break;

		case OM_SETCHILDACTIVE:
			{
				// 将指定的子窗口设置为焦点
				OWindow* pWnd = FindChildByID (wParam);
				if ((pWnd != m_pActive) && (pWnd != NULL))
				{
					// 调用自己的OnKillFocus函数
					OnKillFocus (wParam);
					// 给失去焦点的窗口发送OM_KILLFOCUS通知消息

					O_MSG msg;
					if (m_pActive != NULL)
					{
						msg.pWnd    = m_pActive;
						msg.message = OM_KILLFOCUS;
						msg.wParam  = wParam;
						msg.lParam  = 0;
						m_pApp->SendMsg (&msg);
					}

					// 修改焦点子窗口
					int id = SetFocus (pWnd);

					// 调用自己的OnSetFocus函数
					OnSetFocus (id);
					// 给获得焦点的控件发送OM_SETFOCUS通知消息
					msg.pWnd    = FindChildByID( id );
					msg.message = OM_SETFOCUS;
					msg.wParam  = id;
					msg.lParam  = 0;
					m_pApp->SendMsg (&msg);
					// 向桌面窗口发出重绘父窗口的消息

					UpdateView (m_pParent);
				}
			}
			break;

		case OM_DELETECHILD:
			{
				// 删除指定的子窗口
				OWindow* pChild = FindChildByID (wParam);

				if (pChild != NULL)
				{
					DeleteChild (pChild);

					// 将当前活动窗口设置为指定窗口
					OWindow* pActiveWindow = (OWindow*) lParam;
					if (pActiveWindow != NULL)
					{
						SetFocus (pActiveWindow);

						// 给获得焦点的控件发送OM_SETFOCUS通知消息
						O_MSG msg;
						msg.pWnd    = pActiveWindow;
						msg.message = OM_SETFOCUS;
						msg.wParam  = pActiveWindow->m_ID;
						msg.lParam  = 0;
						m_pApp->SendMsg (&msg);
					}

					// 向桌面窗口发出重绘自己的消息
					UpdateView (this);
				}
			}
			break;

		case OM_KILLFOCUS:
			{
				if (m_nChildCount != 0)
				{
					O_MSG msg;
					msg.message = OM_KILLFOCUS;
					msg.lParam  = 0;
					OWindow* pNext = m_pChildren;
					int i;
					for (i = 0; i < m_nChildCount; i++)
					{
						if (pNext != NULL)
						{
							msg.pWnd   = pNext;
							msg.wParam = pNext->m_ID;
							m_pApp->SendMsg (&msg);
							pNext = pNext->m_pNext;
						}
					}
				}

				OnKillFocus (m_ID);	// 虚函数，处理失去焦点
			}
			break;

		case OM_SETFOCUS:
			{
				// 给焦点发送SetFocus消息
				if (m_pActive != NULL)
				{
					O_MSG msg;
					msg.pWnd    = m_pActive;
					msg.message = OM_SETFOCUS;
					msg.lParam  = m_pActive->m_ID;
					msg.wParam  = 0;
					m_pApp->SendMsg (&msg);
				}

				OnSetFocus (m_ID);	// 虚函数，处理得到焦点
			}
			break;

		default:
			{
			}
		}
	}

	return iReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备（鼠标）消息处理
int OWindow::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = 0;

	if (nMsg == OM_LBUTTONDOWN)
	{
		if (PtInWindow (wParam, lParam))
		{
			if (IsWindowEnabled())
			{
				// 如果本窗口不是焦点，而且父窗口的焦点不是对话框
				// 则将本窗口设置为焦点
				if (m_pParent != NULL)
				{
					OWindow* pParentActive = m_pParent->m_pActive;
					if (pParentActive != NULL)
					{
						if ((pParentActive != this) && (pParentActive->m_wWndType != WND_TYPE_DIALOG))
						{
							O_MSG msg;	
							msg.pWnd    = m_pParent;
							msg.message = OM_SETCHILDACTIVE;
							msg.wParam  = m_ID;
							msg.lParam  = 0;
							m_pApp->SendMsg (&msg);
						}
					}
					else
					{
						O_MSG msg;	
						msg.pWnd    = m_pParent;
						msg.message = OM_SETCHILDACTIVE;
						msg.wParam  = m_ID;
						msg.lParam  = 0;
						m_pApp->SendMsg (&msg);
					}
				}
			}
		}
	}

	// 沿焦点路径发送的消息，如果已经被处理，则后续的窗口不再处理。
	// 首先调用焦点窗口的处理函数
	if (m_pActive != NULL) {
		nReturn = m_pActive->PtProc (pWnd, nMsg, wParam, lParam);
	}

	// 鼠标消息需要广播处理，但只处理一次
	if (nReturn != 1)
	{
		if (nMsg == OM_LBUTTONDOWN)
		{
			OWindow* pNext = m_pChildren;
			int i;
			for (i = 0; i < m_nChildCount; i++)
			{
				if (pNext->PtProc (pWnd, nMsg, wParam, lParam) == 1)
				{
					nReturn = 1;
					break;
				}
				pNext = pNext->m_pNext;
			}
		}
	}

	// 鼠标消息由Scroll处理
	if (nReturn != 1)
	{
		if (nMsg == OM_LBUTTONDOWN)
		{
			int nRet = 0;

			if (m_pVScroll != NULL)
			{
				nRet = m_pVScroll->TestPt (wParam, lParam);
				if (nRet > 0)
				{
					switch (nRet)
					{
					case SCROLL_PT_UP:
						OnScrollUp();
						break;
					case SCROLL_PT_PAGEUP:
						OnScrollPageUp();
						break;
					case SCROLL_PT_DOWN:
						OnScrollDown();
						break;
					case SCROLL_PT_PAGEDOWN:
						OnScrollPageDown();
						break;
					case SCROLL_PT_BUTTON:
						m_pVScroll->RecordPos (lParam);
						m_nMouseMoving = 1;
						break;
					}
					nReturn = 1;
				}
			}

			if ((nRet == 0) && (m_pHScroll != NULL))
			{
				nRet = m_pHScroll->TestPt (wParam, lParam);
				if (nRet > 0)
				{
					switch (nRet)
					{
					case SCROLL_PT_UP:
						OnScrollLeft();
						break;
					case SCROLL_PT_PAGEUP:
						OnScrollPageLeft();
						break;
					case SCROLL_PT_DOWN:
						OnScrollRight();
						break;
					case SCROLL_PT_PAGEDOWN:
						OnScrollPageRight();
						break;
					case SCROLL_PT_BUTTON:
						m_pHScroll->RecordPos (wParam);
						m_nMouseMoving = 2;
						break;
					}
					nReturn = 1;
				}
			}
		}
		else if (nMsg == OM_LBUTTONUP)
		{
			if (m_nMouseMoving > 0)
			{
				// 鼠标左键弹起，取消拖拽
				m_nMouseMoving = 0;
				nReturn = 1;
			}
		}
		else if (nMsg == OM_MOUSEMOVE)
		{
			if (m_nMouseMoving == 1)
			{
				// 处理拖拽
				if (m_pVScroll != NULL)
				{
					int nNewPos = m_pVScroll->TestNewPos (wParam, lParam);
					if (nNewPos != -1)
						OnVScrollNewPos (nNewPos);
					nReturn = 1;
				}
			}
			else if (m_nMouseMoving == 2)
			{
				// 处理拖拽
				if (m_pHScroll != NULL)
				{
					int nNewPos = m_pHScroll->TestNewPos (wParam, lParam);
					if (nNewPos != -1)
						OnHScrollNewPos (nNewPos);
					nReturn = 1;
				}
			}
		}
	}

	return nReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 改变窗口的位置和尺寸
// 设置成功返回TRUE，失败返回FALSE
BOOL OWindow::SetPosition (int x, int y, int w, int h)
{
	m_x = x;
	m_y = y;
	m_w = w;
	m_h = h;

	// 重新设置滚动条的尺寸
	if (m_pVScroll != NULL)
	{
		m_pVScroll->m_x = m_x+m_w-13;
		m_pVScroll->m_y = m_y;
		m_pVScroll->m_w = 13;
		m_pVScroll->m_h = m_h;
	}

	if (m_pHScroll != NULL)
	{
		m_pHScroll->m_x = m_x;
		m_pHScroll->m_y = m_y+m_h-13;
		m_pHScroll->m_w = m_w;
		m_pHScroll->m_h = 13;
	}

	return TRUE;
}

// 获得窗口的位置和尺寸
BOOL OWindow::GetPosition (int* px, int* py, int* pw, int* ph)
{
	*px = m_x;
	*py = m_y;
	*pw = m_w;
	*ph = m_h;
	return TRUE;
}

// 获得窗口的ID号
int OWindow::GetID ()
{
	return m_ID;
}

// 设置窗口的题头
BOOL OWindow::SetText (char* pText, int nLength)
{
	if (pText == NULL) {
		DebugPrintf ("ERROR: OWindow::SetText parameter == NULL!\n");
		return FALSE;
	}

	if (nLength > WINDOW_CAPTION_BUFFER_LEN -1) {
		nLength = WINDOW_CAPTION_BUFFER_LEN -1;
	}

	strncpy (m_sCaption, pText, nLength);
	return TRUE;
}

// 获取窗口的题头
BOOL OWindow::GetText (char* pText)
{
	strncpy (pText, m_sCaption, WINDOW_CAPTION_BUFFER_LEN);
	return TRUE;
}

// 获得窗口题头字符串的长度
int OWindow::GetTextLength ()
{
	return strlen (m_sCaption);
}

// 设置当前处于活动状态（处于焦点）的子窗口
// 注意：应当将当前处于活动状态的子窗口改成非活动的
// 返回实际获得焦点的控件的ID号
int OWindow::SetFocus (OWindow* pWnd)
{
	OWindow* pOld = m_pActive;
	OWindow* pFocus = pWnd;

	int i;
	for (i=0; i<m_nChildCount; i++)
	{
		if (pFocus == NULL) {
			return -1;
		}

		if( pFocus->IsWindowEnabled() )
		{
			if (pOld != pFocus)
			{
				// 使老窗口失去焦点
				if (pOld != NULL)
					{
					pOld->m_wStatus &= ~WND_STATUS_FOCUSED;		// 去除焦点属性
					if( (pOld->m_wWndType == WND_TYPE_BUTTON)
					 || (pOld->m_wWndType == WND_TYPE_IMAGE_BUTTON) )
					{
						// 去除默认属性
						pOld->m_wStatus &= ~WND_STATUS_DEFAULT;
						m_pDefaultButton = FindDefaultButton ();// 将默认按钮设置为原始默认按钮
					}
				}

				// 将子窗口设置为焦点
				m_pActive = pFocus;
				m_pActive->m_wStatus |= WND_STATUS_FOCUSED;		// 给子窗口添加焦点属性
				if( (m_pActive->m_wWndType == WND_TYPE_BUTTON)
				 || (m_pActive->m_wWndType == WND_TYPE_IMAGE_BUTTON) )
				{
					if (m_pDefaultButton != NULL) {
						m_pDefaultButton->m_wStatus &= ~WND_STATUS_DEFAULT;
					}

					m_pActive->m_wStatus |= WND_STATUS_DEFAULT;	// 如果焦点是按钮，再加默认属性
					m_pDefaultButton = m_pActive;				// 并设置为默认按钮
				}
				else
				{
					// 给默认按钮添加默认属性
					OWindow* pWnd = FindDefaultButton ();
					if (pWnd != NULL)
					{
						m_pDefaultButton = pWnd;
						m_pDefaultButton->m_wStatus |= WND_STATUS_DEFAULT;
					}
				}
			}
			return pFocus->m_ID;
		}
		pFocus = pFocus->m_pNext;
	}
	// 没有找到可以被设置为焦点的控件
	m_pActive = NULL;
	return -1;
}

// 在子窗口链表中查找相应ID的窗口，如果找不到则返回NULL
OWindow* OWindow::FindChildByID (int id)
{
	OWindow* pNext = m_pChildren;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if (pNext->m_ID == id)
		{
			return pNext;
		}
		pNext = pNext->m_pNext;
	}
	return NULL;
}

// 在子窗口链表中查找相应tab号的窗口，如果找不到则返回NULL
OWindow* OWindow::FindChildByTab (int iTab)
{
	OWindow* pNext = m_pChildren;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if (pNext->m_nTabOrder == iTab)
		{
			return pNext;
		}
		pNext = pNext->m_pNext;
	}
	return NULL;
}

// 在子窗口链表中查找默认按钮
OWindow* OWindow::FindDefaultButton ()
{
	OWindow* pNext = m_pChildren;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if ((pNext->m_wStyle & WND_STYLE_ORIDEFAULT) > 0)
		{
			return pNext;
		}
		pNext = pNext->m_pNext;
	}
	return NULL;
}

// 查找在位于一个子窗口垂直正下方的子窗口
OWindow* OWindow::FindWindowDown (OWindow* pWnd)
{
	OWindow* pNext = m_pChildren;
	OWindow* pRet  = NULL;
	int i;
	int nTmp;
	int nLen = SCREEN_H * SCREEN_H + SCREEN_W * SCREEN_W;
	for (i = 0; i < m_nChildCount; i++)
	{
		if ((pNext == pWnd)|| ! (pNext->IsWindowEnabled()))
		{
			pNext = pNext->m_pNext;
			continue;
		}

		// 首先查找位于本控件下方的控件
		if ((pNext->m_y > pWnd->m_y) &&
			(abs((pWnd->m_x - pNext->m_x) * 2 + pWnd->m_w - pNext->m_w) < (pWnd->m_w + pNext->m_w)))
		{
			// 再找距离最近的控件
			int dx = pWnd->m_x - pNext->m_x;
			int dy = pWnd->m_y - pNext->m_y;
			nTmp = dx * dx + dy * dy;
			if ((nTmp > 0) && (nTmp < nLen))
			{
				nLen = nTmp;
				pRet = pNext;
			}
		}

		pNext = pNext->m_pNext;
	}

	return pRet;
}

// 查找在位于一个子窗口垂直正上方的子窗口
OWindow* OWindow::FindWindowUp (OWindow* pWnd)
{
	OWindow* pNext = m_pChildren;
	OWindow* pRet  = NULL;
	int i;
	int nTmp;
	int nLen = SCREEN_H * SCREEN_H + SCREEN_W * SCREEN_W;
	for (i = 0; i < m_nChildCount; i++)
	{
		if ((pNext == pWnd) || ! (pNext->IsWindowEnabled()))
		{
			pNext = pNext->m_pNext;
			continue;
		}

		// 首先查找位于本控件上方的控件
		// 判断纵向投影是否相交
		if ((pWnd->m_y > pNext->m_y) &&
			(abs((pWnd->m_x - pNext->m_x) * 2 + pWnd->m_w - pNext->m_w) < (pWnd->m_w + pNext->m_w)))
		{
			// 再找距离最近的控件
			int dx = pWnd->m_x - pNext->m_x;
			int dy = pWnd->m_y - pNext->m_y;
			nTmp = dx * dx + dy * dy;
			if ((nTmp > 0) && (nTmp < nLen))
			{
				nLen = nTmp;
				pRet = pNext;
			}
		}

		pNext = pNext->m_pNext;
	}

	return pRet;
}

// 查找在位于一个子窗口水平正左方的子窗口
OWindow* OWindow::FindWindowLeft (OWindow* pWnd)
{
	OWindow* pNext = m_pChildren;
	OWindow* pRet  = NULL;
	int i;
	int nTmp;
	int nLen = SCREEN_H * SCREEN_H + SCREEN_W * SCREEN_W;
	for (i = 0; i < m_nChildCount; i++)
	{
		if ((pNext == pWnd) || ! (pNext->IsWindowEnabled()))
		{
			pNext = pNext->m_pNext;
			continue;
		}

		// 首先查找位于本控件左方的控件
		// 判断水平投影是否相交
		if ((pWnd->m_x > pNext->m_x) &&
			(abs((pWnd->m_y - pNext->m_y) * 2 + pWnd->m_h - pNext->m_h) < (pWnd->m_h + pNext->m_h)))
		{
			// 再找距离最近的控件
			int dx = pWnd->m_x - pNext->m_x;
			int dy = pWnd->m_y - pNext->m_y;
			nTmp = dx * dx + dy * dy;
			if ((nTmp > 0) && (nTmp < nLen))
			{
				nLen = nTmp;
				pRet = pNext;
			}
		}

		pNext = pNext->m_pNext;
	}

	return pRet;
}

// 查找在位于一个子窗口水平正右方的子窗口
OWindow* OWindow::FindWindowRight (OWindow* pWnd)
{
	OWindow* pNext = m_pChildren;
	OWindow* pRet  = NULL;
	int i;
	int nTmp;
	int nLen = SCREEN_H * SCREEN_H + SCREEN_W * SCREEN_W;
	for (i = 0; i < m_nChildCount; i++)
	{
		if ((pNext == pWnd) || ! (pNext->IsWindowEnabled()))
		{
			pNext = pNext->m_pNext;
			continue;
		}

		// 首先查找位于本控件右方的控件
		// 判断水平投影是否相交
		if ((pNext->m_x > pWnd->m_x) &&
			(abs((pWnd->m_y - pNext->m_y) * 2 + pWnd->m_h - pNext->m_h) < (pWnd->m_h + pNext->m_h)))
		{
			// 再找距离最近的控件
			int dx = pWnd->m_x - pNext->m_x;
			int dy = pWnd->m_y - pNext->m_y;
			nTmp = dx * dx + dy * dy;
			if (nTmp < nLen)
			{
				nLen = nTmp;
				pRet = pNext;
			}
		}

		pNext = pNext->m_pNext;
	}

	return pRet;
}


// 删除一个子窗口
// 相应的要更新各个窗口的tab序号
BOOL OWindow::DeleteChild (OWindow* pChild)
{
	if (m_nChildCount <= 0) {
		DebugPrintf ("ERROR: OWindow::DeleteChild has no child!\n");
		return FALSE;
	}

	if (m_nChildCount == 1)
	{
		m_pChildren		= NULL;
		m_pActive		= NULL;
		m_pDefaultButton= NULL;
		m_nChildCount	= 0;
	}
	else if (m_nChildCount > 1)
	{
		pChild->m_pPrev->m_pNext = pChild->m_pNext;
		pChild->m_pNext->m_pPrev = pChild->m_pPrev;
		m_nChildCount --;

		if (m_pChildren == pChild)
		{
			if (pChild->m_pNext == NULL) {
				return FALSE;
			}
			m_pChildren = pChild->m_pNext;	// 将子窗口列表中的下一个设置为首位
		}

		if (m_pActive == pChild)
		{
			// 将当前窗口后面的控件设为焦点
			OWindow* pWnd = FindChildByTab (pChild->m_nTabOrder +1);
			if (SetFocus (pWnd) == -1) {
				m_pActive = NULL;
			}
		}

		if (m_pDefaultButton == pChild)
		{
			m_pParent->m_pDefaultButton = FindDefaultButton ();
		}

		// 修改控件的tab序号
		int nTab = pChild->m_nTabOrder;
		OWindow* pNext = m_pChildren;
		int i;
		for (i = 0; i < m_nChildCount; i++)
		{
			if (pNext == NULL) {
				return FALSE;
			}

			if (pNext->m_nTabOrder > nTab)
			{
				pNext->m_nTabOrder --;
			}
			pNext = pNext->m_pNext;
		}
	}
	delete pChild;
	return TRUE;
}

// 设置窗口的滚动条
// nWitch = 1：设置右侧的垂直滚动条；
// nWitch = 2：设置下方的水平滚动条；
// 滚动条的尺寸根据窗口尺寸进行设置
// 如果要设置的滚动条并不存在，则创建
BOOL OWindow::SetScrollBar (int nWitch, int nRange, int nSize, int nPos)
{
	if ((m_wStyle & WND_STYLE_NO_SCROLL) > 0) {
		DebugPrintf ("ERROR: OWindow::SetScrollBar window style error!\n");
		return FALSE;
	}

	if (nWitch == 1)
	{
		if (m_pVScroll == NULL)
		{
			// 创建
			m_pVScroll = new OScrollBar ();
			if (m_pHScroll == NULL)
			{
				// 只有这一个滚动条，不必为另一个预留空间
				m_pVScroll->Create (1, m_x+m_w-13, m_y, 13, m_h, nRange, nSize, nPos);
			}
			else
			{
				m_pVScroll->Create (1, m_x+m_w-13, m_y, 13, m_h-13, nRange, nSize, nPos);
				m_pHScroll->m_w -= 13;
			}
		}
		else
		{
			// 设置
			m_pVScroll->SetRange (nRange);
			m_pVScroll->SetSize (nSize);
			m_pVScroll->SetPos (nPos);
		}
		return TRUE;
	}
	else if (nWitch == 2)
	{
		if (m_pHScroll == NULL)
		{
			// 创建
			m_pHScroll = new OScrollBar ();
			if (m_pVScroll == NULL)
			{
				// 只有这一个滚动条，不必为另一个预留空间
				m_pHScroll->Create (2, m_x, m_y+m_h-13, m_w, 13, nRange, nSize, nPos);
			}
			else
			{
				m_pHScroll->Create (2, m_x, m_y+m_h-13, m_w-13, 13, nRange, nSize, nPos);
				m_pVScroll->m_h -= 13;
			}
		}
		else
		{
			// 设置
			m_pHScroll->SetRange (nRange);
			m_pHScroll->SetSize (nSize);
			m_pHScroll->SetPos (nPos);
		}
		return TRUE;
	}

	DebugPrintf ("ERROR: OWindow::SetScrollBar parameter nWitch error!\n");
	return FALSE;
}

// 控制滚动条的显示与消隐
// nScroll = 1：设置垂直滚动条
// nScroll = 2：设置水平滚动条
// 如果要设置的滚动条并不存在，则返回FALSE
BOOL OWindow::ShowScrollBar (int nScroll, BOOL bShow)
{
	if ((m_wStyle & WND_STYLE_NO_SCROLL) > 0) {
		DebugPrintf ("ERROR: OWindow::ShowScrollBar m_wStyle error!\n");
		return FALSE;
	}

	if (nScroll == 1)
	{
		if (m_pVScroll == NULL)
		{
			return FALSE;
		}
		else
		{
			if (bShow)
			{
				m_pVScroll->m_nStatus = 1;
			}
			else
			{
				m_pVScroll->m_nStatus = 0;
			}
			return TRUE;
		}
	}
	else if (nScroll == 2)
	{
		if (m_pHScroll == NULL)
		{
			return FALSE;
		}
		else
		{
			if (bShow)
			{
				m_pHScroll->m_nStatus = 1;
			}
			else
			{
				m_pHScroll->m_nStatus = 0;
			}
			return TRUE;
		}
	}
	
	DebugPrintf ("ERROR: OWindow::ShowScrollBar parameter nScroll error!\n");
	return FALSE;
}

// 向桌面发送重绘窗口的消息
BOOL OWindow::UpdateView (OWindow* pWnd)
{
	O_MSG msg;
	msg.pWnd    = m_pApp->m_pMainWnd;
	msg.message = OM_PAINT;
	msg.wParam  = (ULONGLONG)pWnd;
	msg.lParam  = 0;
	return m_pApp->PostMsg (&msg);
}

// 窗口使能
BOOL OWindow::EnableWindow (BOOL bEnable)
{
	// 如果从Enable转化为Disable，则进行失去焦点的处理
	if (((m_wStatus & WND_STATUS_INVALID) == 0) && (! bEnable))
	{
		O_MSG msg;
		msg.pWnd    = this;
		msg.message = OM_KILLFOCUS;
		msg.wParam  = 0;
		msg.lParam  = 0;
		m_pApp->SendMsg (&msg);
	}

	if (bEnable) {
		m_wStatus &= ~WND_STATUS_INVALID;
	}
	else {
		m_wStatus |=  WND_STATUS_INVALID;
	}

	return TRUE;
}

// 判断窗口是否使能
BOOL OWindow::IsWindowEnabled()
{
	if (((m_wStatus & WND_STATUS_INVALID) == 0) &&
		((m_wStatus & WND_STATUS_INVISIBLE) == 0)) {
		return TRUE;
	}

	return FALSE;
}

// 判断窗口是否可见
BOOL OWindow::IsWindowVisible()
{
	if ((m_wStatus & WND_STATUS_INVISIBLE) == 0) {
		return TRUE;
	}

	return FALSE;
}

// 判断窗口是否顶层对话框
BOOL OWindow::IsTopMostDialog()
{
	if (m_pChildren == NULL)
		return TRUE;

	OWindow* pChild = m_pChildren;
	OWindow* pNext = pChild->m_pNext;
	int i;
	for (i = 0; i < m_nChildCount; i++)
	{
		if (pChild) {
			if (pChild->m_wWndType == WND_TYPE_DIALOG)
				return FALSE;
		}
		if (pNext) {
			pChild = pNext;
			pNext = pChild->m_pNext;
		}
		else {
			break;
		}
	}
	return TRUE;
}

// 判断窗口是否焦点链上的窗口
BOOL OWindow::IsWindowActive()
{
	OWindow* pActive = this;
	OWindow* pParent = m_pParent;

	while (pParent != NULL)
	{
		if (pParent->m_pActive != pActive) {
			return FALSE;
		}

		// 如果焦点关系可以追溯到MainWindow，则返回TRUE
		if (pParent->m_wWndType == WND_TYPE_DESKTOP) {
			return TRUE;
		}

		pActive = pParent;
		pParent = pActive->m_pParent;
	}

	return FALSE;
}

// 等待延时
void OWindow::Delay (int nMilliSecond)
{
	ULONGLONG lasttime = sys_clock ();
	while ((sys_clock() - lasttime) < nMilliSecond)
	{
		if (m_pApp->m_fnEventProcess) {
			// 维持主程序事件循环的运转
			m_pApp->m_fnEventProcess();
		}
		else {
			DebugPrintf("ERROR: ODialog::DoModal m_pApp->m_fnEventProcess is NULL!\n");
#if defined (RUN_ENVIRONMENT_LINUX)
			usleep(1000);
#endif // defined(RUN_ENVIRONMENT_LINUX)
#if defined (RUN_ENVIRONMENT_WIN32)
			Sleep(1);
#endif // defined(RUN_ENVIRONMENT_WIN32)
		}
	};
}

#if defined (MOUSE_SUPPORT)
// 判断一个点是否落在本窗口的范围之内
BOOL OWindow::PtInWindow (int x, int y)
{
	if (PtInRect (x,y,m_x,m_y,m_x+m_w,m_y+m_h)) {
		return TRUE;
	}

	return FALSE;
}
#endif // defined(MOUSE_SUPPORT)

// 定时器消息处理，虚函数，什么也不做
void OWindow::OnTimer (int nTimerID, int nInterval)
{
}

// 窗口创建后的初始化处理
void OWindow::OnInit()
{
}

// 关闭窗口，允许关闭本窗口则返回TRUE，不允许则返回FALSE
BOOL OWindow::OnClose ()
{
	return TRUE;
}

// 子窗口得到焦点
void OWindow::OnSetFocus (int id)
{
}

// 子窗口失去焦点
void OWindow::OnKillFocus (int id)
{
}

// 子窗口数据改变
void OWindow::OnDataChange (int id)
{
}

#if defined (MOUSE_SUPPORT)
// 与滚动条有关的函数
void OWindow::OnScrollUp ()
{
}
void OWindow::OnScrollDown ()
{
}
void OWindow::OnScrollLeft ()
{
}
void OWindow::OnScrollRight ()
{
}
void OWindow::OnScrollPageUp ()
{
}
void OWindow::OnScrollPageDown ()
{
}
void OWindow::OnScrollPageLeft ()
{
}
void OWindow::OnScrollPageRight ()
{
}
void OWindow::OnVScrollNewPos (int nNewPos)
{
}
void OWindow::OnHScrollNewPos (int nNewPos)
{
}
#endif // defined(MOUSE_SUPPORT)

/* END */
