////////////////////////////////////////////////////////////////////////////////
// @file OCheckBox.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OCHECKBOX_H__)
#define __OCHECKBOX_H__


class OCheckBox : public OWindow
{
private:
	enum { self_type = WND_TYPE_CHECK_BOX };

	int m_nCheckState;

public:
	OCheckBox ();
	virtual ~OCheckBox ();

	BOOL Create (OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);
	
	// 虚函数，绘制按钮
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);
#endif // defined(MOUSE_SUPPORT)

	// 设置选择状态
	BOOL SetCheck (int nCheck);

	// 得到选择状态
	int GetCheck ();

private:
	// 处理选择状态改变
	void OnCheck ();
};

#endif // !defined(__OCHECKBOX_H__)
