////////////////////////////////////////////////////////////////////////////////
// @file OMsgBoxDialog.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OMSGBOXDIALOG_H__)
#define __OMSGBOXDIALOG_H__

class OMsgBoxDialog : public ODialog
{
private:
	char m_sInformation[WINDOW_CAPTION_BUFFER_LEN];	// 信息字符串
	WORD m_wMsgBoxStyle;		// 对话框样式

public:
	OMsgBoxDialog ();
	virtual ~OMsgBoxDialog ();

	// 虚函数，绘制对话框
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 创建MessageBox
	BOOL Create (OWindow* pParent, char* sTitle, char* sText, WORD wMsgBoxStyle, int ID);
};

#endif // !defined(__OMSGBOXDIALOG_H__)
