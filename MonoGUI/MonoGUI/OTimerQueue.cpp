////////////////////////////////////////////////////////////////////////////////
// @file OTimerQueue.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OTimerQueue::OTimerQueue ()
{
	m_nCount = 0;
}

OTimerQueue::~OTimerQueue ()
{
	// 删除定时器列表
	RemoveAll ();
}

// 添加一个定时器；
// 如果当前定时器的数量已经达到TIMER_MAX所定义的数目，则返回FALSE；
// 如果发现一个ID与当前定时器相同的定时器，则直接修改该定时器的设定；
BOOL OTimerQueue::SetTimer (OWindow* pWindow, int nTimerID, int interval)
{
	ULONGLONG lNow = sys_clock ();

	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_arTimerQ[i].ID == nTimerID)	// 发现一个ID与当前定时器相同的定时器
		{
			m_arTimerQ[i].pWnd     = pWindow;
			m_arTimerQ[i].interval = interval;
			m_arTimerQ[i].lasttime = lNow;
			return TRUE;
		}
	}

	// 定时器队列满
	if (m_nCount >= TIMER_QUEUE_SIZE) {
		return FALSE;
	}

	// 添加一个定时器
	m_arTimerQ[m_nCount].pWnd     = pWindow;
	m_arTimerQ[m_nCount].ID       = nTimerID;
	m_arTimerQ[m_nCount].interval = interval;
	m_arTimerQ[m_nCount].lasttime = lNow;
	m_nCount ++;
	return TRUE;
}

// 删除一个定时器；
// 根据TimerID删除
BOOL OTimerQueue::KillTimer (int nTimerID)
{
	if (m_nCount <= 0) {
		return FALSE;
	}

	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_arTimerQ[i].ID == nTimerID)		// 发现一个ID与当前定时器相同的定时器
		{
			// 后面的往前搬
			int k = i + 1;
			while (k < m_nCount) {
				memcpy (&(m_arTimerQ[i]), &(m_arTimerQ[k]), sizeof(O_TIMER));
				i++;
				k++;
			}

			m_nCount --;
			return TRUE;
		}
	}

	// 没找到
	return FALSE;
}

// 检查定时器队列；
// 如果发现某个定时器到时了，首先用FindMsg函数检查消息队列中有没有同一个定时器发出的消息，
// 如果没有，则使用PostMsg函数向消息队列中插入MSG_TIMER消息；
// 如果PostMsg向消息队列插入消息失败，则该函数返回FALSE；
BOOL OTimerQueue::CheckTimer (OApp* pApp)
{
	if (m_nCount <= 0) {	// 定时器队列空
		return TRUE;
	}

	ULONGLONG lNow = sys_clock ();

	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if ((lNow - m_arTimerQ[i].lasttime) >= m_arTimerQ[i].interval)
		{
			m_arTimerQ[i].lasttime = lNow;

			O_MSG msg;
			msg.pWnd    = m_arTimerQ[i].pWnd;
			msg.message = OM_TIMER;
			msg.wParam  = m_arTimerQ[i].ID;
			msg.lParam  = m_arTimerQ[i].interval;
			if (! pApp->FindMsg (&msg))
			{
				// 如果在消息队列中发现了完全相同的定时器消息
				// 则表明上一个消息还没有被处理完，不再插入新的消息
				if (! pApp->PostMsg (&msg)) {
					return FALSE;	// 发送消息失败
				}
			}
		}
	}

	return TRUE;	// 所有定时器都没有到达指定时间
}

// 删除所有定时器
BOOL OTimerQueue::RemoveAll ()
{
	m_nCount = 0;
	return TRUE;
}

/* END */
