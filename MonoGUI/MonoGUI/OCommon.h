////////////////////////////////////////////////////////////////////////////////
// @file OCommon.h: global functions.
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OCOMMON_H__)
#define __OCOMMON_H__


// 在Microsoft Windows下TYPE_CHECK是一个很重要的宏，避免在转换窗口类型时搞错了窗口类型
// 所有从OWindow派生子类的成员函数，都应该调用TYPE_CHECK检查
// 该宏定义不影响代码在Linux下的编译
#define WND_TYPE_ERROR   (-100)
#if !defined(DEBUG)
  #define CHECK_TYPE              if (GetWndType() != self_type) return
  #define CHECK_TYPE_RETURN       if (GetWndType() != self_type) return WND_TYPE_ERROR
  #define CHECK_TYPE_RETURN_NULL  if (GetWndType() != self_type) return 0x0
#else
  #define CHECK_TYPE
  #define CHECK_TYPE_RETURN
  #define CHECK_TYPE_RETURN_NULL
#endif

#if !defined(BYTE)
#define BYTE    unsigned char
#endif // !defined(BYTE)
#if !defined(WORD)

#define WORD    unsigned short
#endif // !defined(WORD)
#if !defined(DWORD)

#if defined(RUN_ENVIRONMENT_LINUX)
#define DWORD   unsigned int
#else
#define DWORD   unsigned long
#endif // defined(RUN_ENVIRONMENT_LINUX)
#endif // !defined(DWORD)

#if !defined(ULONGLONG)
#if defined(RUN_ENVIRONMENT_LINUX)
#define ULONGLONG unsigned long long
#else
#define ULONGLONG unsigned __int64
#endif // defined(RUN_ENVIRONMENT_LINUX)
#endif // !defined(ULONGLONG)

#if !defined(BOOL)
#define BOOL    int
#endif // !defined(BOOL)
#if !defined(FALSE)
#define	FALSE	0
#endif // !defined(FALSE)
#if !defined(TRUE)
#define	TRUE	1
#endif // !defined(TRUE)
#if !defined(NULL)
#define	NULL	0x0
#endif // !defined(NULL)

typedef struct _ODATETIME
{
	int nYear;
	int nMonth;
	int nDay;
	int nHour;
	int nMinute;
	int nSecond;
} ODATETIME;

// CLOLRS:
// 0: clear a bit; 
// 1: set a bit;
// 2: keep background color;
// 3: inverse background color
enum
{
	FOUR_COLOR_BLACK = 0,
	FOUR_COLOR_WHITE = 1,
	FOUR_COLOR_TRANSPARENT = 2,
	FOUR_COLOR_INVERSE = 3
};

typedef struct _FOUR_COLOR_IMAGE
{
	int w;
	int h;
	char* buffer;
	_FOUR_COLOR_IMAGE (int nW, int nH, char* buf)
	{
		w = nW;
		h = nH;
		buffer = buf;
	}
} FOUR_COLOR_IMAGE;

typedef struct _BLACK_WHITE_IMAGE
{
	int w;
	int h;
	BYTE* buffer;
	_BLACK_WHITE_IMAGE (int nW, int nH, BYTE* buf)
	{
		w = nW;
		h = nH;
		buffer = buf;
	}
} BW_IMAGE;

// 输入法窗口状态
enum
{
	IME_FORBID = 0,
	IME_CLOSE  = 1,
	IME_OPEN   = 2
};

// 定义窗口类型
enum
{
	WND_VALID_BEGIN       = 99,
	WND_TYPE_DESKTOP      = 100,
	WND_TYPE_DIALOG		  = 101,
	WND_TYPE_STATIC		  = 102,
	WND_TYPE_BUTTON		  = 103,
	WND_TYPE_EDIT		  = 104,
	WND_TYPE_LIST		  = 105,
	WND_TYPE_COMBO		  = 106,
	WND_TYPE_PROGRESS	  = 107,
	WND_TYPE_IMAGE_BUTTON = 108,
	WND_TYPE_CHECK_BOX    = 109,
	WND_TYPE_IME		  = 110,
	WND_VALID_END		  = 111
};

// 定义各种风格
#define WND_STYLE_NORMAL        0x0
#define WND_STYLE_NO_BORDER     0x0001		// 无边框(用于按钮和对话框之外的控件)
#define WND_STYLE_ROUND_EDGE    0x0002		// 圆角(只用于对话框和少数控件)
#define WND_STYLE_PASSWORD      0x0004		// 密码(只用于编辑框)
#define WND_STYLE_GROUP         0x0008		// 组框(只用于静态文本)
#define WND_STYLE_SOLID         0x0010		// 立体效果(可用于任何控件，建议只用于对话框和按钮)
#define WND_STYLE_ORIDEFAULT    0x0020		// 原始默认按钮(只用于按钮)
#define WND_STYLE_NO_TITLE      0x0040		// 无题头(只用于对话框)
#define WND_STYLE_DISABLE_IME   0x0080		// 禁用输入法(只对编辑框和组合框有效)
#define WND_STYLE_NO_SCROLL     0x0100		// 无滚动条(只对列表框和组合框有效)
#define WND_STYLE_AUTO_DROPDOWN 0x0200		// 自动弹出下拉菜单(只对组合框有效)
#define WND_STYLE_AUTO_OPEN_IME 0x0400		// 自动打开拼音输入法
#define WND_STYLE_IGNORE_ENTER  0x0800		// 编辑框不响应回车
#define WND_STYLE_CLOSE_BUTTON  0x1000      // 右上角显示关闭按钮(只用于对话框)

// 定义各种状态
#define	WND_STATUS_NORMAL       0x0
#define WND_STATUS_INVISIBLE    0x0001		// 不可见(不可见同时无效)
#define WND_STATUS_INVALID      0x0002		// 无效(则只显示，不接受消息，不能被设为焦点)
#define WND_STATUS_FOCUSED      0x0004		// 处于焦点的
#define WND_STATUS_DEFAULT      0x0008		// 默认的(只用于按钮)
#define WND_STATUS_CHECKED      0x0010		// 选中的(只用于选择框)
#define WND_STATUS_PUSHED       0x0020		// 被按下的(只用于按钮)

// 定义各种消息
// 系统消息使用OM_开头，自定义消息用MSG_开头
enum
{
	OM_QUIT              = 999,         // 程序退出消息
	OM_CLOSE             = 1000,        // 窗口关闭消息
	OM_TIMER             = 1001,        // 定时器消息
	OM_PAINT             = 1002,        // 绘制消息
	OM_SETCHILDACTIVE    = 1003,        // 设置子窗口成为焦点
	OM_SETFOCUS          = 1004,        // 设置焦点
	OM_KILLFOCUS         = 1005,        // 失去焦点
	OM_DELETECHILD       = 1006,        // 删除子窗口
	OM_DATACHANGE        = 1007,        // 数据改变
	OM_KEYDOWN           = 1008,        // 按键消息
	OM_PUSHDOWN          = 1009,        // 按钮被按下
	OM_CHAR              = 1010,        // 传递字符
	OM_NOTIFY_PARENT     = 1011,        // 通知父窗口
	OM_MSGBOX_RETURN     = 1012,        // MsgBox返回
	OM_INITWINDOW        = 1013,        // 窗口创建后的初始化

#if defined (MOUSE_SUPPORT)
	OM_LBUTTONDOWN       = 1014,        // 鼠标左键按下
	OM_LBUTTONUP         = 1015,        // 鼠标左键弹起
	OM_MOUSEMOVE         = 1016,        // 鼠标移动
#endif // defined(MOUSE_SUPPORT)

	OM_USER              = 1200
};

// 定义各种默认ID
enum
{
	OID_MSG_BOX         = 10009,
	OID_OK              = 10010,
	OID_CANCEL          = 10011,
	OID_CLOSE           = 10012,
	OID_YES             = 10013,
	OID_NO              = 10014,

	//用户自定义消息的ID起始值
	OID_USER            = 10200,

	//定义默认消息框的ID
	OID_DLG_MSG_DEFAULT = 20000
};

// 对各个类进行预声明
class KeyMap;          // 按键宏定义与按键值对照表
class BWImgMgt;        // 图像资源管理类
class LCD;             // 液晶屏操作函数(GAL层)
class OAccell;         // 快捷键
class OCaret;          // 脱字符
class OScrollBar;      // 滚动条
class OMsgQueue;       // 消息队列
class OTimerQueue;     // 定时器队列
class OWindow;         // 窗口
class OStatic;         // 静态文本控件
class OButton;         // 按钮控件
class OEdit;           // 编辑框
class OList;           // 列表框
class OCombo;          // 组合框
class OProgressBar;    // 进度条
class OImgButton;      // 图像按钮
class OCheckBox;       // 复选框
class OIME;            // 输入法窗口
class OIME_DB;         // 输入法数据库
class ODialog;         // 对话框
class OMsgBoxDialog;   // MsgBox对话框
class OApp;            // 主程序
class OClock;          // 钟表界面
class OSystemBar;      // 系统状态条
//

// 定义全局函数

// 判断是否闰年
BOOL IsLeapYear (int year);

// 日期增加一天
void NextDay (ODATETIME* pdt);

// 从二色buffer中获得某个点的颜色值
int BWImgGetPixel (BYTE* buffer, int buf_w, int buf_h, int x, int y);

// 获得汉字在字库中的偏移量
int GetHzIndex (BYTE cH, BYTE cL, int nLen);

// 判断一个点是否落在一个矩形区域内
BOOL PtInRect (int x, int y, int left, int top, int right, int bottom);

// 统计字符串有多少行
int TotalLines(char* sString, BOOL bIgnoreBlankLines = TRUE);

// 从字符串中取得一行
BOOL GetLine(char* sString, char* sReturn, int nReturnLimit, int k);

// 从字符串中取得一行
BOOL GetLine2(char* sString, int* pnStartPos, int* pnLength, int k);

// 从字符串中取得两个“;”之间的部分
BOOL GetSlice (char* sString, char* sReturn, int nReturnLimit, int k);

// MonoGUI系统时钟
ULONGLONG sys_clock();

// 弹出一个消息框
void OMsgBox (OWindow* pWnd, char* sTitle, char* sContent, WORD wStyle, int ID);

// 弹出一个模态的消息框
int OModalMsgBox (OWindow* pWnd, char* sTitle, char* sContent, WORD wStyle, int ID);

// 判断当前位置前面(或后面)的文字是否汉字
// 是汉字返回TRUE，不是汉字返回FALSE
// cString:字符串
// iPos:当前位置的索引(第几个字节)
// bMode:TRUE检查后面的两个字节;FALSE检查前面的两个字节
BOOL IsChinese (char* sString, int nPos, BOOL bMode);

// 判断当前位置是否塞在了汉字的两个字节中间
// 塞在中间返回FALSE，否则返回TRUE
BOOL IsPosValid (char* sString, int nPos);

// 得到一个字符串的显示长度
int GetDisplayLength (char* sString, int nLength);

// 给出一个宽度和一个字符串，得到可以显示的字节数
int GetDisplayLimit (char* sString, int nWidth);

// 给出对齐模式下，该宽度允许显示的字节数
int GetDisplayLimit_Align (char* sString, int sWidth);

// 窗口类型转字符串
char* GetWindowTypeName(int nWndType);

#if defined (RUN_ENVIRONMENT_WIN32)
void win32_DebugPrintf(const char* format, ...);
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 下面定义MonoGUI中用到的各种资源
// 向上箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Arror_Up;
// 向下箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Arror_Down;
// 向左箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Arror_Left;
// 向右箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Arror_Right;
// 向上空心箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Hollow_Arror_Up;
// 向下空心箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Hollow_Arror_Down;
// 向左空心箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Hollow_Arror_Left;
// 向右空心箭头(7*7)
extern FOUR_COLOR_IMAGE g_4color_Hollow_Arror_Right;
// 小向上箭头(5*5)
extern FOUR_COLOR_IMAGE g_4color_Little_Arror_Up;
// 小向下箭头(5*5)
extern FOUR_COLOR_IMAGE g_4color_Little_Arror_Down;
// 小向左箭头(5*5)
extern FOUR_COLOR_IMAGE g_4color_Little_Arror_Left;
// 小向右箭头(5*5)
extern FOUR_COLOR_IMAGE g_4color_Little_Arror_Right;
// 滚动条中间的滑动按钮(11*11)
extern FOUR_COLOR_IMAGE g_4color_Scroll_Button;
// 圆角按钮—正常(13*13)
extern FOUR_COLOR_IMAGE g_4color_Button_Normal;
// 圆角按钮—默认(13*13)
extern FOUR_COLOR_IMAGE g_4color_Button_Default;
// 错误图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Error;
// 感叹号图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Exclamation;
// 问号图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Question;
// 信息图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Information;
// 忙图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Busy;
// 打印机图标(23*23)
extern FOUR_COLOR_IMAGE g_4color_Icon_Printer;
// 选中状态的复选框图标(13*13)
extern FOUR_COLOR_IMAGE g_4color_Check_Box_Selected;
// 未选中状态的复选框图标(13*13)
extern FOUR_COLOR_IMAGE g_4color_Check_Box_Unselected;
// 方角关闭按钮(15*15)
extern FOUR_COLOR_IMAGE g_4color_Close_Button1;
// 圆角关闭按钮(15*15)
extern FOUR_COLOR_IMAGE g_4color_Close_Button2;
// 时钟按钮(15*15)
extern FOUR_COLOR_IMAGE g_4color_Clock_Button;
// 电源
extern FOUR_COLOR_IMAGE g_4color_Power;
// 电池
extern FOUR_COLOR_IMAGE g_4color_Battery;
// 大写字母
extern FOUR_COLOR_IMAGE g_4color_Captial;
// 小写字母
extern FOUR_COLOR_IMAGE g_4color_Lowercase;
// 九宫格小键盘
extern BW_IMAGE g_bw_SudokoKeyboard_Lowercase;
extern BW_IMAGE g_bw_SudokoKeyboard_Uppercase;
//

// OCombo控件
#define COMBO_DISABLE_EDIT				0x00000001
#define COMBO_DISABLE_DROPDOWN			0x00000002
#define COMBO_UN_DROPPED				0
#define COMBO_DROPPED					1
#define COMBO_PUBHDOWN_BUTTON_WIDTH		11	// EDIT右侧小按钮的默认像素宽度
#define COMBO_DEFAULT_LIST_HEIGHT		42	// LIST控件的默认像素高度(三行)
#define COMBO_ID_EDIT					100	// EDIT控件的ID号
#define COMBO_ID_LIST					101	// LIST控件的ID号

// 对话框
#define OMB_ERROR            0x0001		// 显示错误图标
#define OMB_EXCLAMATION      0x0002		// 显示感叹号图标
#define OMB_QUESTION         0x0004		// 显示问号图标
#define OMB_INFORMATION      0x0008		// 显示信息图标
#define OMB_BUSY             0x0010		// 显示漏壶图标
#define OMB_PRINT            0x0020		// 显示打印机图标
#define OMB_ROUND_EDGE       0x0040		// 圆角风格
#define OMB_SOLID            0x0080		// 立体风格
#define OMB_YESNO            0x0100		// 显示“是”“否”按钮
#define OMB_OKCANCEL         0x0200		// 显示“确定”“取消”按钮
#define OMB_DEFAULT_NO       0x0400
// 如果对话框的样式是MB_YESNO或MB_OKCANCEL，则对话框创建后默认的焦点是“是”按钮
// 或者“确定”按钮。如果使用“否”和“取消”为默认按钮，则指定MB_DEFAULT_NO样式
#define OMB_ENGLISH          0x0800
// 按钮文字显示为英文  确定：OK  取消：CANCEL  是：YES  否：NO

// 表盘
#define DEFAULT_CLOCK_X 39
#define DEFAULT_CLOCK_Y 19
extern FOUR_COLOR_IMAGE g_4color_ClockFace;
// 定义表针的中心
#define CENTER_X 33
#define CENTER_Y 45
// 定义时针和分针的终点
extern char HOUR_HAND[12][2];
extern char MINUTE_HAND[60][2];
//

#endif // !defined(__OCOMMON_H__)
